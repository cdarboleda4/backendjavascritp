const express = require("express");
const router = express.Router();

const { Usuario } = require("../model/usuario");
router.post("/", async (req, res) => {
   let usuario = await Usuario.findOne({ correo: req.body.correo });
   if (usuario) return res.status(400).send("El usuario existe en la BD");
   usuario = new Usuario({
      nombre: req.body.nombre,
      correo: req.body.correo,
      contrasena: req.body.contrasena,
   });
   const result = await usuario.save();
   const jwtToken = usuario.generateJWT();
   res.status(200).send({ jwtToken });
});
module.exports = router;