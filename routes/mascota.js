const express = require("express");
const router = express.Router();
const { Usuario } = require("../model/usuario");
const { Mascota } = require("../model/mascota");

const auth = require("../middleware/auth");
router.post ("/",auth,async(req,res) =>{
   const  usuario  =  await  Usuario.findById(req.usuario._id);
   if (!usuario) return res.status(400).send("El usuario no existe");
   let  mascotaTipo  =  await  Mascota.findOne({ tipo: req.body.tipo });
   if (mascotaTipo) return res.status(400).send("El tipo de mascota ya existe inserta otro");
   const mascota = new Mascota({
       idUsuario: usuario._id,
       nombre: req.body.nombre,
       tipo: req.body.tipo,
       descripcion: req.body.descripcion,
   });
   const result = await mascota.save();
   res.status(200).send(result);
});
module.exports = router;