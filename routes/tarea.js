const express = require("express");
const router = express.Router();
const { Usuario } = require("../model/usuario");
const { Tarea } = require("../model/tarea");
const auth = require("../middleware/auth");
router.post("/", auth, async (req, res) => {
    const usuario = await Usuario.findById(req.usuario._id);
    if (!usuario) return res.status(400).send("El usuario no existe");
    const tarea = new Tarea({
        idUsuario: usuario._id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        estado: req.body.estado,
    });
    const result = await tarea.save();
    res.status(200).send(result);
});
router.get("/lista", auth, async (req, res) => {
    const usuario = await Usuario.findById(req.usuario._id);
    if (!usuario) return res.status(400).send("El usuario no existe!!");
    const tarea = await Tarea.find({ idUsuario: req.usuario._id });
    res.send(tarea);
});
router.put("/", auth, async (req, res) => {
    const usuario = await Usuario.findById(req.usuario._id);
    if (!usuario) return res.status(400).send("El usuario no existe!!");
    const tarea = await Tarea.findByIdAndUpdate(
        req.body._id,
        {
            idUsuario: usuario._id,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            estado: req.body.estado,
        },
        {
            new: true,
        }
    );
    if (!tarea) return res.status(400).send("No hay tareas");
    res.status(200).send(tarea);
});
router.delete("/:_id", auth, async (req, res) => {
    const usuario = await Usuario.findById(req.usuario._id);
    if (!usuario) return res.status(400).send("El usuario no existe!!");
    const tarea = await Tarea.findOneAndDelete(req.params._id);
    if (!tarea) return res.status(400).send("No hay tareas asignadas");
    res.status(200).send({ message: "Tarea Eliminada" });
});

module.exports = router;