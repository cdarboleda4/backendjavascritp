const express = require("express");
const router = express.Router();
const { Usuario } = require("../model/usuario");

router.post("/", async (req, res) => {
    let usuario = await Usuario.findOne({ correo: req.body.correo });
    if (!usuario) return res.status(400).send("Datos invalidos");
    if (usuario.contrasena != req.body.contrasena) return res.status(400).send("Datos invalidos");

    const jwtToken = usuario.generateJWT();
    res.status(200).send({ jwtToken });
});
module.exports = router;


