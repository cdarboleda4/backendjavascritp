const mongoose =  require("mongoose");

const esquemaTarea = new mongoose.Schema({
    idUsuario:String,
    nombre:String,
    descripcion:String,
    estado:String,
    fecha:{
        type:Date,
        default: Date.now,
    },
});

const Tarea = mongoose.model("tarea",esquemaTarea);
module.exports.Tarea = Tarea;