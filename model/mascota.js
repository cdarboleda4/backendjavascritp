const mongoose = require("mongoose");

const esquemaMascota = new mongoose.Schema({

    idUsuario: String,
    nombre: String,
    tipo: String,
    descripcion: String,
});

const Mascota = mongoose.model("mascota", esquemaMascota);
module.exports.Mascota = Mascota;