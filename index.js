const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const usuario = require("./routes/usuario");
const auth = require("./routes/auth");
const tarea = require("./routes/tarea");
const mascota = require("./routes/mascota");
const app = express();
app.use(express.json());
app.use("/api/usuario",usuario);
app.use("/api/auth",auth);
app.use("/api/tarea",tarea);
app.use("/api/mascota",mascota);

const port = process.env.port || 3004;
app.listen(port,() => console.log("escuchando el puerto: " +port));

//definir funcion mongoose le decimos cual es nuestro punto de conexion (base de datos)
mongoose.connect("mongodb://localhost/tareasjs",{
    useNewUrlParser   :true,
    useFindAndModify  :false,
    useCreateIndex    :true,
    useUnifiedTopology: true,

})

.then(() => console.log("conexion con mongo Ok!!"))
.catch((error) => console.log("Fallo la conexión!!" + error));
